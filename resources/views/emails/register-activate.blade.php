
@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            UTA Radio
        @endcomponent
    @endslot

    {{-- Body --}}
# Hello  {!! $user['user_name'] !!},<br>

¡Bienvenido a UTA Radio! Por favor da clic en el siguiente enlace para confirma tu cuenta:<br />
@component('mail::button', ['url' =>  $user['activationUrl']  ])
    Activar Cuenta
@endcomponent


    Gracias,

    {{-- Footer --}}
    @slot('footer')
    @component('mail::footer')
    &copy; 2020 Algunos Derechos Reservados
@endcomponent
@endslot
@endcomponent