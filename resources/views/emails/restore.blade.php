@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            UTA Radio
        @endcomponent
    @endslot

    {{-- Body --}}
    # Hola  {!! $user['user_name'] !!},<br>

¡Bienvenido a UTA Radio! Da clic en el siguiente link para reestablecer tu cuenta:<br />
@component('mail::button', ['url' =>  $user['activationUrl']  ])
        reestablecer Cuenta
@endcomponent


    Gracias,

    {{-- Footer --}}
    @slot('footer')
    @component('mail::footer')
    &copy; 2020 Algunos Derechos Reservados
@endcomponent
@endslot
@endcomponent