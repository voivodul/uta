

@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
           UTA Radio
        @endcomponent
    @endslot

    {{-- Body --}}
# Hola

Hemos Recibido un nuevo contacto por email.<br />
**Nombre :** {{ $data['contact-name'] }}<br />
**Email :** {{ $data['contact-email'] }}<br />
**Mensaje :** {{ $data['contact-msg'] }}

Gracias,

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
           &copy; 2020 Algunos Derechos Reservados
        @endcomponent
    @endslot
@endcomponent
