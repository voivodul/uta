@extends('layouts/default')

{{-- Page title --}}
@section('title')
Nosotros
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/aboutus.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/animate/animate.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/owl_carousel/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/owl_carousel/css/owl.theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/devicon/devicon.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/devicon/devicon-colors.css') }}">
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
   
    @stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container">
        <!-- Slider Section Start -->
        <div class="row my-3">
            <!-- Left Heading Section Start -->
            <div class="col-md-7 col-sm-12  col-md-12 col-lg-8 wow bounceInLeft pantalla" data-wow-duration="5s">
                <h2><label>Quienes somos:</label></h2>
                <p>
                    UTA RADIO es una empresa especializada en servicios de comunicación radiofónica en vivo por internet, somos la única empresa que hoy ofrece un concepto innovador (llamado ALTERGROUND), ofreciendo el mejor y mas completo servicio de radiodifusión las 24 horas del día sin interrupción, somos una experiencia integral de comunicación entre locutores, músicos, patrocinadores, clientes y radioescuchas.
                </p>
                <p>
                    Desde nuestro comienzo hemos tenido mas de 200 bandas , grupos etc. Mas de 22 eventos al año de alto impacto en el medio, mas de 40 locutores nacionales e internacionales, mas de 5 redes sociales con influencia en mas de 200 grupos en redes sociales con no menos de 10000 miembros cada una, cubriendo eventos y promocionándolos de manera continua.

<p>Contamos con una amplia agenda de grupos, proyectos y artistas en todos los ámbitos de influencia ALTERGROUND.</p>

<p>Hoy por hoy UTA RADIO cuenta con la mas amplia y confiable oferta comercial para nuestros clientes, que ofrecen satisfacer la demanda de los consumidores.
                </p>
                <p>
                    A lo largo de nuestra historia nos hemos caracterizado por innovar en términos de radiodifusión, ofreciendo a los radio escuchas una amplia gama de programas y música de mas de 50 géneros, UTA RADIO ofrece un foro para actividades diversas, expos, cultura, música, conciertos y demás a precios por debajo de cualquier competidor gracias a nuestro principal patrocinador UTA CLUB.</p>

 <p>Con estos servicios nuestra cobertura se incrementa cada día en el mercado , hasta llegar ahora al sector de Televisión por Internet. Llegando así, a mucha mas gente con el propósito de posicionarnos en el mercado como el pilar mas fuerte de la radio por internet.
                </p>

                <h2><label>Misión</label></h2>

                <p>Ser una familia global, diversa y alterna, con una herencia de orgullo en la alternancia cultural, con pasión por ofrecer a nuestros radio escuchas diversas maneras de ver nuestro entorno sociocultural y alterno musical, proporcionando un servicio excepcional de radio por internet con calidad y respeto irrestricto a la sociedad, en donde la música, filosofía, imaginación, terror y discusión, etc. se ofrecen como expectativa de empresa democrática y rentable para sus anunciantes y locutores dirigido a un amplio sector de la población.
                </p>

                <h2><label>Visión</label></h2>

                <p>Convertirnos en la empresa principal de mundo de radiodifusión por internet.
                </p>

                <h2><label>Valores</label></h2>

                <p>Hacer bien las cosas para nuestra gente, nuestro ambiente y nuestra sociedad, pero sobretodo para nuestros radio escuchas y clientes.
                </p>

                <h2><label>Objetivos</label></h2>

                <p>Establecer continuamente las normas más altas de satisfacción del radio escucha y cliente en nuestra industria, a través del profesionalismo y servicios innovadores.

<p>Asegurar la posición competitiva más fuerte en nuestros mercados relevantes, a través del diseño creativo de programas y una excelencia operativa.</p>

<p>Asociarnos con los mejores proveedores y productores del mundo, fortaleciendo oportunidades de negocios por Internet para brindar valor agregado tanto a UTA RADIO como a radio escuchas y clientes.</p>

<p>Ser reconocidos como una radio difusora de primera elección.</p>

<p>Somos una organización multicultural. Nuestra meta es facultar a nuestros colaboradores en todos los niveles, e integrarlos plenamente a nuestra red de trabajo global.</p>

<p>Incrementar selectivamente nuestra presencia mundial.</p>

<p>Demostrar continuamente nuestro compromiso hacia un desempeño democrático, ambiental, tecnológico sustentables y representar visiblemente un papel de liderazgo en la responsabilidad social dentro de nuestro ámbito de influencia, (la radio difusión).</p>

<p>Mantener un diálogo activo con gobiernos, organizaciones internacionales y organizaciones no gubernamentales, y ser reconocidos como un medios de comunicación aliado valioso y confiable.</p>

<p>Tener un sano desempeño financiero de largo plazo y las acciones más atractivas en nuestra industria.
                </p>
            </div>
            <!-- //Left Heaing Section End -->
            <!-- Slider Start -->
            <div class="col-md-12 col-sm-12  col-lg-4 slider wow fadeInRightBig" data-wow-duration="5s">
                <div id="owl-demo" class="owl-carousel owl-theme">
                    <div class="item"><img src="{{ asset('images/image_16.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('images/image_17.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('images/image_16.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                </div>
            </div>
            <!-- //Slider End -->
        </div>
        <!-- //Slider Section End -->
        <!-- Our Team Section Start -->
        <div class="text-center my-3">
            <h3><span class="heading_border bg-danger" >Our Team</span></h3>
        </div>
        <div class="row">
            <!-- Our Team Heading Start -->
            <!-- //Our Team Heading End -->
            <!-- Image1 Section Start -->
            <div class="col-md-6 col-sm-12 col-12  col-lg-3 profile wow zoomIn" data-wow-duration="3.5s">
                <div class="thumbnail bg-white  text-center">
                    <img src="{{ asset('images/img_3.jpg') }}" alt="team-image" class="img-fluid">
                    <div class="caption">
                        <b>John Doe</b>
                        <br /> Founder &amp; Partner
                        <br />
                        <div class="divide">
                            <a href="#" class="divider"> <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795" data-hc="#3a5795"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee" data-hc="#55acee"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="google-plus" data-size="22" data-loop="true" data-c="#d73d32" data-hc="#d73d32"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="linkedin" data-size="22" data-loop="true" data-c="#1b86bd" data-hc="#1b86bd"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //Image1 Section End -->
            <!-- Image2 Section Start -->
            <div class="col-md-6 col-sm-12 col-12 col-lg-3 profile wow zoomIn" data-wow-duration="3s" data-wow-delay="0.8s">
                <div class="thumbnail bg-white text-center">
                    <img src="{{ asset('images/img_5.jpg') }}" alt="team-image" class="img-fluid">
                    <div class="caption">
                        <b>Francina Steinberg</b>
                        <br /> CEO
                        <br />
                        <div class="divide">
                            <a href="#" class="divider"> <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795" data-hc="#3a5795"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee" data-hc="#55acee"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="google-plus" data-size="22" data-loop="true" data-c="#d73d32" data-hc="#d73d32"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="linkedin" data-size="22" data-loop="true" data-c="#1b86bd" data-hc="#1b86bd"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //Image2 Section End -->
            <!-- Image3 Section Start -->
            <div class="col-md-6 col-sm-12 col-12 col-lg-3  profile wow zoomIn" data-wow-duration="3s" data-wow-delay="0.8s">
                <div class="thumbnail bg-white  text-center">
                    <img src="{{ asset('images/img_4.jpg') }}" alt="team-image" class="img-fluid">
                    <div class="caption">
                        <b>Audrey Sheldon</b>
                        <br /> Executive Manager
                        <br />
                        <div class="divide">
                            <a href="#" class="divider"> <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795" data-hc="#3a5795"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee" data-hc="#55acee"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="google-plus" data-size="22" data-loop="true" data-c="#d73d32" data-hc="#d73d32"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="linkedin" data-size="22" data-loop="true" data-c="#1b86bd" data-hc="#1b86bd"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Image3 Section End -->
            <!-- Image4 Section Star -->
            <div class="col-md-6 col-sm-5 col-12  col-lg-3 profile wow zoomIn" data-wow-duration="3s" data-wow-delay="0.8s">
                <div class="thumbnail bg-white text-center">
                    <img src="{{ asset('images/img_6.jpg') }}" alt="team-image" class="img-fluid">
                    <div class="caption">
                        <b>Sam Bellows</b>
                        <br /> Manager
                        <br />
                        <div class="divide">
                            <a href="#" class="divider"> <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795" data-hc="#3a5795"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee" data-hc="#55acee"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="google-plus" data-size="22" data-loop="true" data-c="#d73d32" data-hc="#d73d32"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="linkedin" data-size="22" data-loop="true" data-c="#1b86bd" data-hc="#1b86bd"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //Image4 Section End -->
        </div>
        <!-- //Our Team Section End -->
    </div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- page level js starts-->
    <script type="text/javascript" src="{{ asset('vendors/owl_carousel/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/wow/js/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontend/carousel.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontend/aboutus.js') }}"></script>
    <!--page level js ends-->
@stop
