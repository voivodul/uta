

@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            UTA Radio
        @endcomponent
    @endslot

    {{-- Body --}}

    # Hola  {!! $user['user_name'] !!},<br>

    Da clic en el siguiente enlace para reestablecer tu contraseña
@component('mail::button', ['url' =>  $user['forgotPasswordUrl'] ])
        Reestablecer Password
@endcomponent


    Gracias,

    {{-- Footer --}}
    @slot('footer')
    @component('mail::footer')
    &copy; 2020 Algunos Derechos Reservados
@endcomponent
@endslot
@endcomponent
