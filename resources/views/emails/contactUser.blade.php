
@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            UTA Radio
        @endcomponent
    @endslot
    {{-- Body --}}

 # Hola {{ $data['contact-name'] }}

¡Bienvenido a UTA Radio! Hemos recibido los siguientes datos.<br />
Los datos proveidos son:<br />
**Nombre :** {{ $data['contact-name'] }}<br />
**Email :** {{ $data['contact-email'] }}<br />
**Mensaje :** {{ $data['contact-msg'] }}

¡Gracias por contactar a UTA Radio! Te contestaremos en breve.

Saludos,

    {{-- Footer --}}
    @slot('footer')
    @component('mail::footer')
     &copy; 2020 Algunos Derechos Reservados
@endcomponent
@endslot
@endcomponent
