<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <title>
        @section('title')
        | UTA Radio Generación Alterground
        @show
    </title>
    <link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">
    <!--global css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/lib.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
    <style>
      .dropdown-item:active{
            background-color: transparent !important;
        }
      .indexpage.navbar-nav >.nav-item .nav-link:hover {
          color: #01bc8c;
      }
    </style>
    <!--end of global css-
    <!--page level css-->
    @yield('header_styles')
    <!--end of page level css-->
</head>

<body>
<!-- Header Start -->
<header>
    <!--Icon Section Start-->
    <div class="utaplayer pantalla" id="container">
        <audio class="mejs__player" id="player2" accesskey="p" tabindex="1" controls="" autoplay="true" muted="true" width="100%" height="60">
             <source src="http://167.114.173.72:8168/;" type="audio/mpeg">
        </audio>
    </div>
    <div class="icon-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-8 col-md-4 mt-2">
                    <ul class="list-inline">
                        <li>
                            <a href="https://www.facebook.com/UTARADIO/" target="_blank"> <i class="livicon" data-name="facebook" data-size="18" data-loop="true" data-c="#fff"
                                            data-hc="#757b87"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/UtaRadioTw" target="_blank"> <i class="livicon" data-name="twitter" data-size="18" data-loop="true" data-c="#fff"
                                            data-hc="#757b87"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UCmrfnnMjt99BitbDKXLqcdg" target="_blank"> <i class="livicon" data-name="youtube" data-size="18" data-loop="true" data-c="#fff"
                                            data-hc="#757b87"></i>
                            </a>
                        </li>
                        </ul>
                </div>
                <div class="col-lg-8 col-4 col-md-8 text-right mt-2">
                    <ul class="list-inline">
                        <li>
                            <a href="mailto:utaradiolive@gmail.com"><i class="livicon" data-name="mail" data-size="18" data-loop="true"
                                                 data-c="#fff"
                                                 data-hc="#fff"></i></a>
                            <label class="d-none d-md-inline-block d-lg-inline-block d-xl-inline-block">
                                <a
                                    href="mailto:utaradiolive@gmail.com"
                                    class="text-white">utaradiolive@gmail.com</a>
                                </label>
                        </li>
                        <li>
                            <a href="https://wa.me/5215527473583"><i class="livicon" data-name="phone" data-size="18" data-loop="true"
                                              data-c="#fff"
                                              data-hc="#fff"></i></a>
                            <label class="d-none d-md-inline-block d-lg-inline-block d-xl-inline-block">
                                <a href="https://wa.me/5215527473583" class="text-white">55 27473583</a></label>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container indexpage">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="{{ route('home') }}">
                <img src="{{ asset('images/logo.png') }}" alt="logo" class="headerlogo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto  margin_right">
                    <li class=" nav-item dropdown {!! (Request::is('aboutus') || Request::is('timeline') || Request::is('faq') || Request::is('blank_page')  ? 'active' : '') !!}">
                        <a href="{{ URL::to('nosotros') }}" class="nav-link"> Nosotros</a>
                       {{--  <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ URL::to('aboutus') }}" class="dropdown-item">About Us</a>
                            </li>
                            <li><a href="{{ URL::to('timeline') }}" class="dropdown-item">Timeline</a></li>
                            <li><a href="{{ URL::to('price') }}" class="dropdown-item">Price</a>
                            </li>
                            <li><a href="{{ URL::to('404') }}" class="dropdown-item">404 Error</a>
                            </li>
                            <li><a href="{{ URL::to('500') }}" class="dropdown-item">500 Error</a>
                            </li>
                            <li><a href="{{ URL::to('faq') }}" class="dropdown-item">FAQ</a>
                            </li>
                            <li><a href="{{ URL::to('blank_page') }}" class="dropdown-item">Blank</a>
                            </li>
                        </ul> --}}
                    </li>
                    <li class="nav-item dropdown {!! (Request::is('portfolio') || Request::is('portfolioitem') ? 'active' : '') !!}">
                        <a href="#" class="nav-link"> Programas</a>
                       {{--  <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ URL::to('portfolio') }}">Horarios</a>
                            </li>
                        </ul> --}}
                    </li>
                    {{-- <li class="nav-item {!! (Request::is(
                    'news*') ? 'active' : '') !!}"><a href="{{ URL::to('news') }}" class="nav-link">News</a>
                    </li>
                    <li class="nav-item {!! (Request::is(
                    'blog') || Request::is('blogitem/*') ? 'active' : '') !!}"><a href="{{ URL::to('blog') }}" class="nav-link">
                    Blog</a>
                    </li>
                    <li class="nav-item {!! (Request::is(
                    'contact') ? 'active' : '') !!}"><a href="{{ URL::to('contact') }}" class="nav-link">Contacto</a>
                    </li> --}}

                    {{--based on anyone login or not display menu items--}}
                    @if(Sentinel::guest())
                   {{--  <li class="nav-item"><a href="{{ URL::to('login') }}" class="nav-link">Ingresar</a>
                    </li>
                    <li class="nav-item"><a href="{{ URL::to('register') }}" class="nav-link">Registrar</a>
                    </li> --}}
                    @else
                    <li class="nav-item {{ (Request::is('my-account') ? 'active' : '') }}"><a href="{{ URL::to('my-account') }}" class="nav-link">Mi
                        Cuenta</a>
                    </li>
                    <li class="nav-item"><a href="{{ URL::to('logout') }}" class="nav-link">Cerrar Sesión</a>
                    </li>
                    @endif
                </ul>
            </div>
        </nav>
        <!-- Nav bar End -->
    </div>
</header>

<!-- //Header End -->

<!-- slider / breadcrumbs section -->
@yield('top')

<!-- Content -->
@yield('content')

<!-- Footer Section Start -->
<footer>
    <div class=" container">
        <div class="footer-text">
            <!-- About Us Section Start -->
            <div class="row">
                <div class="col-sm-4 col-lg-4 col-md-4 col-12">
                    <h4>Acerca de</h4>
                    <p>
                        UTA RADIO es una empresa especializada en servicios de comunicación radiofónica en vivo por internet, somos la única empresa que hoy ofrece un concepto innovador (llamado ALTERGROUND), ofreciendo el mejor y mas completo servicio de radiodifusión las 24 horas del día sin interrupción, somos una experiencia integral de comunicación entre locutores, músicos, patrocinadores, clientes y radioescuchas
                    </p>
                    <hr id="hr_border2">
                    <h4 class="menu">Síguenos</h4>
                    <ul class="list-inline mb-2">
                        <li>
                            <a href="https://www.facebook.com/UTARADIO/" target="_blank"> <i class="livicon" data-name="facebook" data-size="18" data-loop="true"
                                            data-c="#ccc" data-hc="#ccc"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/UtaRadioTw" target="_blank"> <i class="livicon" data-name="twitter" data-size="18" data-loop="true"
                                            data-c="#ccc" data-hc="#ccc"></i>
                            </a>
                        </li>
                       
                        <li>
                            <a href="https://www.youtube.com/channel/UCmrfnnMjt99BitbDKXLqcdg" target="_blank"> <i class="livicon" data-name="youtube" data-size="18" data-loop="true"
                                            data-c="#ccc" data-hc="#ccc"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- //About us Section End -->
                <!-- Contact Section Start -->
                <div class="col-sm-4 col-lg-4 col-md-4 col-12">
                    <h4>Contáctanos</h4>
                    <ul class="list-unstyled">
                        <li>Insurgentes nte. #134</li>
                        <li>Alcaldía Cuáhutemoc</li>
                        <li>CDMX, MEX</li>
                        <li><i class="livicon icon4 icon3" data-name="cellphone" data-size="18" data-loop="true"
                               data-c="#ccc" data-hc="#ccc"></i>Teléfono: 55 27473583
                        </li>
                        <li><i class="livicon icon3" data-name="mail-alt" data-size="20" data-loop="true" data-c="#ccc"
                               data-hc="#ccc"></i> Email:<span class="text-danger" style="cursor: pointer;">
                        utaradiolive@gmail.com</span>
                        </li>
                        <li><i class="livicon icon4 icon3" data-name="music" data-size="18" data-loop="true"
                               data-c="#ccc" data-hc="#ccc">
                               
                               </i> Tunein:<a href="https://tunein.com/radio/UTA-Radio-s252466/" target="_blank">
                            <span class="text-danger" style="cursor: pointer;">Utaradio</span></a>
                        </li>
                    </ul>
                    <hr id="hr_border">
                    <div class="news menu">
                        <h4>News letter</h4>
                        <p>Suscríbete a nuestro Newsletter y ponte al tanto de los últimos evento de UtaRadio</p>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="yourmail@mail.com"
                                   aria-describedby="basic-addon2">
                            <a href="#" class="btn btn-danger text-white" role="button">Suscríbete</a>
                        </div>
                    </div>
                </div>
                <!-- //Contact Section End -->
                <!-- Recent post Section Start -->
                <div class="col-sm-4 col-lg-4 col-md-4 col-12">
                    <h4>Recent Posts</h4>
                    <div class="media">
                        <img class="media-object rounded-circle mr-3" src="{{ asset('images/image_14.jpg') }}"
                             alt="image">
                        <div class="media-body">
                            <p class="media-heading text-justify">Lorem Ipsum is simply dummy text of the printing and type setting
                                industry dummy.</p>
                            <p class="text-right"><i>Sam Bellows</i></p>
                        </div>
                    </div>
                    <div class="media">
                        <img class="media-object rounded-circle mr-3" src="{{ asset('images/image_15.jpg') }}"
                             alt="image">

                        <div class="media-body">
                            <p class="media-heading text-justify">Lorem Ipsum is simply dummy text of the printing and type setting
                                industry dummy.</p>
                            <p class="text-right"><i>Emilly Barbosa Cunha</i></p>
                        </div>
                    </div>
                    <div class="media">
                        <img class="media-object rounded-circle mr-3" src="{{ asset('images/image_13.jpg') }}"
                             alt="image">
                        <div class="media-body">
                            <p class="media-heading text-justify">Lorem Ipsum is simply dummy text of the printing and type setting
                                industry dummy.</p>
                            <p class="text-right"><i>Sinikka Oramo</i></p>
                        </div>
                    </div>
                    <div class="media">
                        <img class="media-object rounded-circle mr-3" src="{{ asset('images/c1.jpg') }}"
                             alt="image">

                        <div class="media-body">
                            <p class="media-heading text-justify">Lorem Ipsum is simply dummy text of the printing and type setting
                                industry dummy.</p>
                            <p class="text-right"><i>Samsa Parras</i></p>
                        </div>
                    </div>
                    <!-- //Recent Post Section End -->
                </div>
            </div>
        </div>
    </div>
<!-- //Footer Section End -->
<div class=" col-12 copyright">
    <div class="container">
        <p>Algunos derechos Reservados Copyright &copy; Paola Ochoa y UTA Radio, {{ date('Y') }}</p>
    </div>
</div>
</footer>
<a id="back-to-top" href="#" class="btn btn-danger btn-lg back-to-top" role="button" data-original-title="Regresar arriba"
   data-toggle="tooltip" data-placement="left">
    <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
</a>



<!--global js starts-->
<script type="text/javascript" src="{{ asset('js/frontend/lib.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/mediaelement-and-player.min.js') }}"></script>
<!--global js end-->
<!-- begin page level js -->
@yield('footer_scripts')
<!-- end page level js -->
<script>
            document.addEventListener('click', musicPlay);
            var audiostr = document.getElementById("player2");

            function musicPlay() {
                audiostr.muted = false; 
                audiostr.volume = 0.5; 
                audiostr.play(); 
                document.removeEventListener('click', musicPlay);
            }
            
        </script>
<script>
    $(".navbar-toggler-icon").click(function () {
        $(this).closest('.navbar').find('.collapse').toggleClass('collapse1')
    })

    $(function () {
        $('[data-toggle="tooltip"]').tooltip().css('font-size', '14px');
    })
</script>
</body>

</html>
