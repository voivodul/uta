@extends('layouts/default')

{{-- Page title --}}
@section('title')
Home
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css starts-->
<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/tabbular.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/animate/animate.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/jquery.circliful.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/owl_carousel/css/owl.carousel.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/owl_carousel/css/owl.theme.css') }}">

<style>
    .box{
        margin-top:53px !important;
    }

</style>

<!--end of page level css-->
@stop

{{-- slider --}}
@section('top')
<!--Carousel Start -->
<div id="owl-demo" class="owl-carousel owl-theme">
    <div class="item img-fluid" style="position: relative;">
         <picture>
            <source media="(min-width: 650px)" srcset="{{ asset('images/slide_2.png') }}">
            <img src="{{ asset('images/slide_2b.png') }}" alt="slider-image"/>
        </picture> 
        <p class="media-heading text-justify" style="position: absolute; right:10%; top: 70%; width: 300px; transform: translateY(-50%);">Lorem Ipsum is simply dummy text of the printing and type setting industry dummy. Lorem Ipsum is simply dummy text of the printing and type setting industry dummy.</p>
    </div>
    <div class="item img-fluid">
        <picture>
            <source media="(min-width: 650px)" srcset="{{ asset('images/slide_2.png') }}">
            <img src="{{ asset('images/slide_2b.png') }}" alt="slider-image"/>
        </picture> 
        <p class="media-heading text-justify" style="position: absolute; right:10%; top: 70%; width: 300px; transform: translateY(-50%);">Lorem Ipsum is simply dummy text of the printing and type setting industry dummy. Lorem Ipsum is simply dummy text of the printing and type setting industry dummy.</p>
    </div>
    <div class="item img-fluid">
        <picture>
            <source media="(min-width: 650px)" srcset="{{ asset('images/slide_2.png') }}">
            <img src="{{ asset('images/slide_2b.png') }}" alt="slider-image"/>
        </picture> 
        <p class="media-heading text-justify" style="position: absolute; right:10%; top: 70%; width: 300px; transform: translateY(-50%);">Lorem Ipsum is simply dummy text of the printing and type setting industry dummy. Lorem Ipsum is simply dummy text of the printing and type setting industry dummy.</p>
    </div>
</div>
<!-- //Carousel End -->
@stop

{{-- content --}}
@section('content')
<!-- Layout Section Start -->
<section class="feature-main">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-12 col-lg-9 wow zoomIn" data-wow-duration="2s">
                <div class="layout-image">
                    <img src="{{ asset('images/layout.png') }}" alt="layout" class="img-fluid"/>
                </div>
            </div>
            <div class="col-md-4 col-lg-3 col-sm-4 col-12 wow lightSpeedIn" data-wow-duration="2s">
                <ul class="list-unstyled  text-right layout-styl">
                    <li>
                        <i class="livicon" data-name="checked-on" data-size="20" data-loop="true" data-c="#01bc8c"
                           data-hc="#01bc8c"></i> Anunciate aquí
                    </li>
                    <li><i class="livicon" data-name="checked-on" data-size="20" data-loop="true" data-c="#01bc8c"
                           data-hc="#01bc8c"></i> Llega a cientos de personas
                    </li>
                    <li><i class="livicon" data-name="checked-on" data-size="20" data-loop="true" data-c="#01bc8c"
                           data-hc="#01bc8c"></i> Expande tu público
                    </li>
                    <li><i class="livicon" data-name="checked-on" data-size="20" data-loop="true" data-c="#01bc8c"
                           data-hc="#01bc8c"></i> Presencia en web
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- //Layout Section Start -->
<!-- Our Team Start -->
<div class="container">
    <div class="row text-center">
        <div class="col-12 my-3">
            <h3><span class="heading_border bg-danger">Programas</span></h3></div>
    </div>
    <div class="row text-center">
        <div class="col-md-6 col-sm-6 col-12 col-lg-3 profile wow" data-wow-duration="3s"
             data-wow-delay="0.5s">
            <div class="thumbnail bg-white">
               <a href="#"><img src="{{ asset('images/img_3.jpg') }}" alt="team-image" class="img-fluid"></a>
                <div class="caption">
                    <b>Programa 1</b>
                    <p class="text-center"> Conductores</p>
                    <div class="divide">
                        <a href="#" class="divider">
                            <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795"
                               data-hc="#3a5795"></i>
                        </a>
                        <a href="#">
                            <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee"
                               data-hc="#55acee"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-3 col-12 profile wow" data-wow-duration="3s"
             data-wow-delay="0.5s">
            <div class="thumbnail bg-white">
                <img src="{{ asset('images/img_5.jpg') }}" alt="team-image">
                <div class="caption">
                    <b>Programa 2</b>
                    <p class="text-center"> Conductor 2 </p>
                    <div class="divide">
                        <a href="#" class="divider">
                            <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795"
                               data-hc="#3a5795"></i>
                        </a>
                        <a href="#">
                            <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee"
                               data-hc="#55acee"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-3 col-12 profile wow" data-wow-duration="3s"
             data-wow-delay="0.5s">
            <div class="thumbnail bg-white">
                <img src="{{ asset('images/img_4.jpg') }}" alt="team-image" class="img-fluid">
                <div class="caption">
                    <b>Programa 1</b>
                    <p class="text-center"> Conductor 3 </p>
                    <div class="divide">
                        <a href="#" class="divider">
                            <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795"
                               data-hc="#3a5795"></i>
                        </a>
                        <a href="#">
                            <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee"
                               data-hc="#55acee"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-3 col-12 profile wow" data-wow-duration="3s"
             data-wow-delay="0.5s">
            <div class="thumbnail bg-white">
                <img src="{{ asset('images/img_6.jpg') }}" alt="team-image">
                <div class="caption">
                    <b>Programa 4</b>
                    <p class="text-center"> Conductor 5 </p>
                    <div class="divide">
                        <a href="#" class="divider">
                            <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795"
                               data-hc="#3a5795"></i>
                        </a>
                        <a href="#">
                            <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee"
                               data-hc="#55acee"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //Our Team End --> 
    </div>

<!-- //Container End -->
@stop
{{-- footer scripts --}}
@section('footer_scripts')
<!-- page level js starts-->
<script type="text/javascript" src="{{ asset('js/frontend/jquery.circliful.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendors/wow/js/wow.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendors/owl_carousel/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/frontend/carousel.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/frontend/index.js') }}"></script>
<!--page level js ends-->
@stop


